require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

const gameType = OpenXum.Yavalax.GameType.STANDARD;
const color = OpenXum.Yavalax.Color.WHITE;
const moveT = OpenXum.Yavalax.MoveType.PUT_STONE;

const coordinates = new OpenXum.Yavalax.Coordinates(0,0);
const move = new OpenXum.Yavalax.Move(moveT,color,coordinates);

describe('Coordinates', () => {

    test('constructor',() => {
        expect(coordinates._column).toEqual(0);
        expect(coordinates._line).toEqual(0);

    });

    test('to string', () => {
        const str = coordinates.to_string();

        expect(str).toEqual(expect.any(String));
        expect(str).toEqual('0 0');
    });

    test('is_valid', () => {
        const invalidCoor = new OpenXum.Yavalax.Coordinates(12,0);
        expect(invalidCoor.is_valid()).toBe(true);
    });

    test('clone', () => {
        const CoordClone = coordinates.clone();

        expect(CoordClone).toEqual(new OpenXum.Yavalax.Coordinates(0,0));
    });
});

describe('Move', () => {

    test('constructor',() => {

        expect(move._type).toBe(moveT);
        expect(move._color).toBe(color);
        expect(move._coordinates).toEqual(coordinates);

    });

    test('encode',() => {
        const encodeMove = new OpenXum.Yavalax.Move(0,1,new OpenXum.Yavalax.Coordinates(1,1));
        const str = encodeMove.encode();

        expect(str).toEqual(expect.any(String));
        expect(str).toEqual('PB0101');
    });

    test('decode',() => {
        const decodeMove = new OpenXum.Yavalax.Move(0,1,new OpenXum.Yavalax.Coordinates(1,1));

        decodeMove.decode(move.encode());

        expect(decodeMove._type).toBe(moveT);
        expect(decodeMove._color).toBe(color);
        expect(decodeMove._coordinates).toEqual(coordinates);
    });

    test('to_object', () => {
        const data = move.to_object();

        expect(data.type).toBe(moveT);
        expect(data.color).toBe(color);
        expect(data.coord).toBe(coordinates);

    });

    test('from_object', () => {
        const objectMove = new OpenXum.Yavalax.Move(0,1,new OpenXum.Yavalax.Coordinates(1,1));
        const data = move.to_object();

        objectMove.from_object(data);

        expect(data.type).toBe(moveT);
        expect(data.color).toBe(color);
        expect(data.coord).toBe(coordinates);

    });

    test('to string',() => {
        const str = move.to_string();

        expect(str).toEqual(expect.any(String));
        expect(str).toEqual('Put white piece at 0 0');
    });
});

describe('Engine', () => {

    test('constructor',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        expect(engine._gameBoard.length).toBe(13);
        expect(engine._gameBoard[1].length).toBe(13);
        expect(engine._gameBoard[1][1]).toBe(OpenXum.Yavalax.Color.NONE);
    });

    test('clone',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);
        const EngineClone = engine.clone();

        expect(EngineClone._gameBoard.length).toBe(engine._gameBoard.length);
        expect(EngineClone._gameBoard[1].length).toBe(engine._gameBoard[1].length);
        expect(EngineClone._gameBoard[1][1]).toBe(engine._gameBoard[1][1]);
    });

    test('to_string',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);
        console.log(engine.to_string());
    });

    test('_put_stone',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);
        engine._put_stone(coordinates);

        expect(engine._gameBoard[0][0]).toBe(color);
        expect(engine._white_stones).toBe(84);
        expect(engine._turn).toBe(1);
    });

    test('check_direction',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(2,0));

        console.log(engine.to_string());

        const cell = new OpenXum.Yavalax.Coordinates(0,0);
        console.log(engine.check_direction(cell,0,1).color);

        expect(engine.check_direction(cell,0,1)).toBe(3);
    });

    test('check_align',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        engine._put_stone(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(2,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(3,0));

        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,1));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,2));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,3));

        engine._put_stone(coordinates);
        expect(engine.check_align(coordinates)).toBe(true);

    });

    test('winner_is',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        engine._put_stone(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(2,0));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(3,0));

        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,1));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,2));
        engine._put_stone(new OpenXum.Yavalax.Coordinates(0,3));

        engine._put_stone(coordinates);
        expect(engine.winner_is()).toBe(color);

        const engineturn =  new OpenXum.Yavalax.Engine(gameType,color);
        engineturn._turn = 167;

        engineturn._put_stone(coordinates);
        expect(engineturn.winner_is()).toBe(OpenXum.Yavalax.Color.EQUAL);

        const engineNoWin =  new OpenXum.Yavalax.Engine(gameType,color);
        expect(engineNoWin.winner_is()).toBe(OpenXum.Yavalax.Color.NONE);

    });

    test('switch_player',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);
        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,0)));
        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,1)));

        expect(engine.current_color()).toBe(OpenXum.Yavalax.Color.BLUE);
    });

    test('get_possible_move_list',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        const listM = engine.get_possible_move_list();
        
    });

    test('play_same',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);
        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,0)));
        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,1)));

        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,0)));
        engine.move(new OpenXum.Yavalax.Move(OpenXum.Yavalax.MoveType.PUT_STONE,engine.current_color(),new OpenXum.Yavalax.Coordinates(0,1)));

        expect(engine.current_color()).toBe(OpenXum.Yavalax.Color.BLUE);
    });

    test('move',() => {
        const engine =  new OpenXum.Yavalax.Engine(gameType,color);

        engine.move(move);

        expect(engine._gameBoard[0][0]).toBe(color);
        expect(engine._white_stones).toBe(84);
        expect(engine._turn).toBe(1);

        engine.move(new OpenXum.Yavalax.Move(moveT,color, new OpenXum.Yavalax.Coordinates(1,0)));

        expect(engine.current_color()).toBe(OpenXum.Yavalax.Color.BLUE);
    });
});