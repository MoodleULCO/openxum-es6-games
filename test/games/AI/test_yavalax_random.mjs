require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let white_victory = 0, blue_victory = 0;

for (let i = 0; i < 100; i += 1) {
    let e = new OpenXum.Yavalax.Engine(OpenXum.Yavalax.GameType.STANDARD, OpenXum.Yavalax.Color.WHITE);
    let p1 = new AI.Generic.RandomPlayer(OpenXum.Yavalax.Color.WHITE, OpenXum.Yavalax.Color.BLUE, e);
    let p2 = new AI.Generic.RandomPlayer(OpenXum.Yavalax.Color.BLUE, OpenXum.Yavalax.Color.WHITE, e);
    let p = p1;
    let moves = [];

    while (!e.is_finished()) {
        const move = p.move();

        moves.push(move);
        e.move(move);
        p = p === p1 ? p2 : p1;
    }

    if (e.winner_is() === 0) {
        white_victory += 1;
    } else {
        blue_victory += 1;
    }

    /*for (let index = 0; index < moves.length; ++index) { //mouvements d'une partie
        console.log(moves[index].to_string());
    }*/
    console.log("Winner is " + (e.winner_is() === OpenXum.Yavalax.Color.WHITE ? "white" : "blue"));
}

console.log("Random white player: " + white_victory + " wins.");
console.log("Random blue player: " + blue_victory + " wins.");

