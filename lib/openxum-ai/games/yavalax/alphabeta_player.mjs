"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';
import Engine from "../../../openxum-core/games/yavalax/engine.mjs";
import Coordinates from "../../../openxum-core/games/yavalax/coordinates.mjs";


class AlphaBetaPlayer extends AlphaBeta {
    constructor(c, o, e) {
        super(c, o, e, 1, 1000);
    }

    evaluate(e, depth) {
        // If the game is finished, return final score
        if (e.is_finished()) {
            switch (e.winner_is()) {
                case this.color():
                    return this.VICTORY_SCORE;
                case this.opponent_color():
                    return -this.VICTORY_SCORE;
                default:
                    return 0;
            }
        }

        // Else check if row are connected
        let score = 0;
        for (let i = 0; i < 13; i++) {
            for (let j = 0; j < 13; j++) {
                const cell = new Coordinates(i, j);

                if (e._gameBoard[cell.get_line()][cell.get_column()] === this.color()) {
                    score += 1;
                    score += e.check_direction(cell, 1, 0) + e.check_direction(cell, -1, 0) - 2;
                    score += e.check_direction(cell, 0, 1) + e.check_direction(cell, 0, -1) - 2;
                    score += e.check_direction(cell, -1, -1) + e.check_direction(cell, 1, 1) - 2;
                    score += e.check_direction(cell, -1, 1) + e.check_direction(cell, 1, -1) - 2;
                }
                if (e._gameBoard[cell.get_line()][cell.get_column()] === this.opponent_color()) {
                    score -= 1;
                    score -= e.check_direction(cell, 1, 0) + e.check_direction(cell, -1, 0) - 2;
                    score -= e.check_direction(cell, 0, 1) + e.check_direction(cell, 0, -1) - 2;
                    score -= e.check_direction(cell, -1, -1) + e.check_direction(cell, 1, 1) - 2;
                    score -= e.check_direction(cell, -1, 1) + e.check_direction(cell, 1, -1) - 2;
                }

            }
        }
        return score;

    }
}


export default AlphaBetaPlayer;