"use strict";

import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._selected_cell = null;
        this._move = undefined;
        this._selected_color = Yavalax.Color.NONE;
    }

//public methods
    draw() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#6BA3FE";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        this._draw_grid();
        this._context.strokeStyle = "#000000";
        this._draw_state();
        this._draw_possible_moves();
    }

    get_move() {
        return new Yavalax.Move(Yavalax.MoveType.PUT_STONE, this._engine.current_color(),
                new Yavalax.Coordinates(this._selected_cell.get_line(), this._selected_cell.get_column()));
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
    }

    unselect() {
        this._selected_cell = null;
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._deltaX = (this._canvas.width * 0.94 - 40) / 13;
        this._deltaY = (this._canvas.height * 0.94 - 40) / 13;
        this._offsetX = this._deltaX;
        this._offsetY = this._deltaY;

        this._scaleX = this._canvas.height / this._canvas.offsetHeight;
        this._scaleY = this._canvas.width / this._canvas.offsetWidth;

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });

        this.draw();
    }

//private methods
    _compute_coordinates(x, y) {
        return new Yavalax.Coordinates(Math.floor((x - this._offsetX) / this._deltaX),
            Math.floor((y - this._offsetY) / this._deltaY));
    }

    _draw_grid() {
        this._context.lineWidth = 1;
        this._context.fillStyle = "#C9FEFE";
        for (let i = 0; i < 13; ++i) {
            for (let j = 0; j < 13; ++j) {
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                this._context.fill();
            }
        }
    }

    _draw_possible_moves() {
        const list = this._engine.get_possible_move_list();
         for (let index = 0; index < list.length; ++index) {
                 const move = list[index];
                 const coordinates = move.get_coord();
                 const x = this._offsetX + this._deltaX / 2 + coordinates.get_line() * this._deltaX;
                 const y = this._offsetY + this._deltaY / 2 + coordinates.get_column()  * this._deltaY;

                 this._context.lineWidth = 1;
                 this._context.strokeStyle = "#000000";
                 this._context.fillStyle = this.engine().current_color() === Yavalax.Color.WHITE ? 'white' : 'blue';

                 this._context.beginPath();
                 this._context.arc(x, y, this._deltaX / 10, 0.0, 2 * Math.PI, false);
                 this._context.closePath();
                 this._context.stroke();
                 this._context.fill();
         }
    }

    _draw_state() {
        let pt;
        this._draw_piece_number();

        for (let x = 0; x < 13; x++) {
            for (let y = 0; y < 13; y++) {
                if (this._engine._gameBoard[x][y] !== Yavalax.Color.NONE) {
                    pt = new Yavalax.Coordinates(x,y);
                    this._draw_piece(pt.get_line(), pt.get_column(), this._engine._gameBoard[x][y]);
                }
            }
        }
    }

    _draw_piece(x, y, piece) {
        let radius = (this._deltaX / 2.5);

        if (piece === Yavalax.Color.BLUE) {
            this._context.strokeStyle = "#000000";
            this._context.fillStyle = "#0000FF";
        } else if (piece === Yavalax.Color.WHITE) {
            this._context.strokeStyle = "#000000";
            this._context.fillStyle = "#ffffff";
        }
        this._context.lineWidth = 1;

        this._context.beginPath();
        this._context.arc (x * this._deltaX + this._offsetX + (this._deltaX / 2) -1,
            y * this._deltaY + this._offsetY + (this._deltaY / 2) -1, radius, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();
    }

    _draw_piece_number() {
        this._context.fillStyle = "#ffffff";
        this._context.textAlign = "start";
        this._context.textBaseline = "middle";
        this._context.font = 0.5 * this._offsetY + "px calibri";
        this._context.textAlign = "end";
        this._context.fillText("Remaining pieces: Blue: " + this._engine._blue_stones +
            "  white: " + this._engine._white_stones, this._width - this._offsetX, this._offsetY / 2);
    }

    _get_click_position(e) {
        const rect = this._canvas.getBoundingClientRect();

        return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);
            let ok = false;

            this._selected_cell = this._compute_coordinates(pos.x, pos.y);

            if (this._selected_cell.is_valid()) {
                ok = true;
            }
            if (ok) {
                this._manager.play();
            }
        }

    }
} //fin classe

export default Gui;