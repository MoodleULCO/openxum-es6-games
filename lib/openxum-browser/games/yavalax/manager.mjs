"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new Yavalax.Move();
    }

    get_current_color() {
        return this._engine.current_color() === Yavalax.Color.WHITE ? 'white' : 'blue';
    }

    get_name() {
        return 'Yavalax';
    }

    get_winner_color() {
        return this._engine.winner_is() === Yavalax.Color.WHITE ? 'white' : 'blue';
    }

    process_move() {}
}

export default Manager;
