"use strict";

import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';
import AI from '../../../openxum-ai/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.Specific.Yavalax.AlphaBetaPlayer
        },
        colors: {
            first: Yavalax.Color.WHITE,
            init: Yavalax.Color.WHITE,
            list: [
                {key: Yavalax.Color.WHITE, value: 'white'},
                {key: Yavalax.Color.BLUE, value: 'blue'}
            ]
        },
        modes: {
            init: Yavalax.GameType.STANDARD,
            list: [
                {key: Yavalax.GameType.STANDARD, value: '0'},
            ]
        },
        opponent_color(color) {
            return color === Yavalax.Color.WHITE ? Yavalax.Color.BLUE : Yavalax.Color.WHITE;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};