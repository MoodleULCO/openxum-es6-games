"use strict";

const Color = {NONE: -1, WHITE: 0, BLUE: 1, EQUAL: 2};

export default Color;
