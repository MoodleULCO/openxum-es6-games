import OpenXum from '../../openxum/index.mjs';
import MoveType from './move_type.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Move from './move.mjs';

class Engine extends OpenXum.Engine {
    constructor(type, color) {
        super();
        this._type = type;
        this._current_color = color;
        this._white_stones = 85;
        this._blue_stones = 85;
        this._last_move = new Move(MoveType.PUT_STONE, Color.NONE,new Coordinates(-1,-1));
        this._turn_player = 0;

        this._gameBoard = new Array(13);
        for(let i = 0; i < 13; i++){
            this._gameBoard[i] = new Array(13);
            for(let j = 0; j < 13; j++) {
                this._gameBoard[i][j] = Color.NONE;
            }
        }

        this._turn = 0;
    }

    build_move() {
        return new Move();
    }

    clone() {
        let game_clone = new Engine(this._type, this._current_color);

        game_clone._attribute = this._attribute;

        return game_clone;
    }

    current_color() {
        return this._current_color;
    }

    get_name() {
        return 'Yavalax';
    }

    get_possible_move_list() {
        let list = [];
        let coord;
        const color = this.current_color();

        for(let i = 0; i < 13; i++){
            for(let j = 0; j < 13; j++){
                coord = new Coordinates(i,j);

                if(!this.is_finished() && this._gameBoard[i][j] === Color.NONE
                    && coord.is_valid() && !this.check_fifth_align(coord)
                    && this.count_align(coord) !== 1){

                    list.push(new Move(MoveType.PUT_STONE,color,coord));
                }
            }
        }
        return list;
    }

    is_finished() {
        return this.winner_is() !== -1;
    }

    move(move) {
        if (move.get_type() === MoveType.PUT_STONE) {
            this._put_stone(move.get_coord());

            if(!this.is_finished()){
                if(this._turn_player === 2 && this.current_color() === Color.BLUE){
                    this._current_color = Color.WHITE;
                    this._turn_player = 0;
                }else if(this._turn_player === 2 && this.current_color() === Color.WHITE){
                    this._current_color = Color.BLUE;
                    this._turn_player = 0;
                }
            }
        }
    }

    parse(str) {
        // TODO
    }

    to_string(){
        let str = '  ';

        for (let _column = 0; _column < this._gameBoard.length; ++_column) {
            str += '  ' + String.fromCharCode('A'.charCodeAt(0) + _column) + ' ';
        }

        str += '\n';

        for (let _line = 0; _line < this._gameBoard.length; ++_line) {
            if (_line < 9) {
                str += (_line + 1) + '  ';
            } else {
                str += (_line + 1) + ' ';
            }
            for (let _column = 0; _column < this._gameBoard.length; ++_column) {
                str += ' ' +this._gameBoard[_line][_column]+ ' ';
            }
            str += '\n';
        }
        return str;
    }

    check_direction(cell, horizontal_offset, vertical_offset) {
        let number_align = 1;
        let column = cell.get_column();
        let line = cell.get_line();
        //const color = this._turn_player === 1 ? this.current_color() : (this.current_color() === Color.WHITE ? Color.BLUE : Color.WHITE);
        const color = this.current_color();

        while( ((column + horizontal_offset) >= 0 && (column + horizontal_offset) < 13) &&
               ((line + vertical_offset) >= 0 && (line + vertical_offset) < 13) &&
                this._gameBoard[line + vertical_offset][column + horizontal_offset] === color ) {
            number_align++;
            column = column + horizontal_offset;
            line = line + vertical_offset;
        }
        return number_align;
    }

    check_align(cell) {
        if(this.count_align(cell) >= 2){
            return true;
        }
        return false;
    }

    check_fifth_align(cell) {
        const align_line = this.check_direction(cell, 1, 0) + this.check_direction(cell, -1, 0) -1;
        const align_column = this.check_direction(cell, 0, 1) + this.check_direction(cell, 0, -1) -1;
        const align_first_diag = this.check_direction(cell, -1, -1) + this.check_direction(cell, 1, 1) -1;
        const align_second_diag = this.check_direction(cell, -1, 1) + this.check_direction(cell, 1, -1) -1;

        if(align_line >= 5 || align_column >= 5 || align_first_diag >= 5 || align_second_diag >= 5){
            return true;
        }
        return false;
    }

    count_align(cell) {
        const align_line = this.check_direction(cell, 1, 0) + this.check_direction(cell, -1, 0) -1;
        const align_column = this.check_direction(cell, 0, 1) + this.check_direction(cell, 0, -1) -1;
        const align_first_diag = this.check_direction(cell, -1, -1) + this.check_direction(cell, 1, 1) -1;
        const align_second_diag = this.check_direction(cell, -1, 1) + this.check_direction(cell, 1, -1) -1;

        return (align_line === 4) + (align_column === 4) + (align_first_diag === 4) + (align_second_diag === 4);
    }

    winner_is() {
        if ((this._blue_stones === 0 && this._white_stones === 0) || this._turn === 168) {
            return Color.EQUAL;
        } else if(this._turn === 0) {
            return Color.NONE;
        } else if(this.check_align(this._last_move.get_coord()) === true) {
            return this._last_move.get_color();
        }
        return Color.NONE;
    }

    in_possible_move(cell) {
        const list = this.get_possible_move_list();

        for (let index = 0; index < list.length; index ++) {
            if (list[index].get_coord().equals(cell)) {
                return true;
            }
        }
        return false;
    }

    _put_stone(selected_cell) {

        if(this.in_possible_move(selected_cell) && selected_cell.is_valid()
            && this._gameBoard[selected_cell.get_line()][selected_cell.get_column()] === Color.NONE) {

            this._gameBoard[selected_cell.get_line()][selected_cell.get_column()] = this.current_color();
            if(this.current_color() === Color.BLUE){
                this._blue_stones--;
            }else{
                this._white_stones--;
            }
            this._last_move = new Move(MoveType.PUT_STONE, this.current_color(),selected_cell);
            this._turn ++;
            this._turn_player++;
        }
    }
}

export default Engine