"use strict";

import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Coordinates from './coordinates.mjs';
import Color from './color.mjs';

class Move extends OpenXum.Move {
    constructor(type, color, coord) {
        super();
        this._type = type;
        this._color = color;
        this._coordinates = coord;
    }

    decode(str) {
        if(str.charAt(0) === 'P') {
            this._type = MoveType.PUT_STONE;
        }
        this._color = (str.charAt(1) === 'W') ?
            Color.WHITE : (str.charAt(1) === 'B' ?
                Color.BLUE : (str.charAt(1) === 'N') ? Color.NONE : Color.EQUAL);
        this._coordinates = new Coordinates(parseInt(str.substr(2,2)), parseInt(str.substr(4,2)));
    }

    encode() {
        return "P" +
            (this._color === Color.WHITE ?
                'W' : (this._color === Color.BLUE ?
                    'B' : (this._color === Color.NONE ? 'N' : 'E'))) +
            (
            (this._coordinates.get_line() > 9 ? '' : '0' + this._coordinates.get_line()) +
            (this._coordinates.get_column() > 9 ? '' : '0' + this._coordinates.get_column())
        );
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._coordinates = new Coordinates(data.coord.get_line(), data.coord.get_column());
    }

    to_object() {
        return {type: this._type, color: this._color, coord: this._coordinates};
    }

    to_string() {
        return 'Put ' + (this._color === Color.WHITE ? 'white' : (this._color === Color.BLUE ? 'blue' : 'invalid color')) + ' piece at ' + this._coordinates.to_string();
    }

    get_type() {
        return this._type;
    }

    get_coord() {
        return this._coordinates;
    }

    get_color() {
        return this._color;
    }
}

export default Move;
