"use strict";

class Coordinates {
    constructor(l,c) {
        this._column = c;
        this._line = l;
    }

    is_valid() {
        if(this._column >= 0 && this._column < 13 && this._line >= 0 && this._line < 13){
            return true;
        }
        return false;
    }

    clone() {
        return new Coordinates(this._column, this._line);
    };

    get_column() {
        return this._column;
    };

    get_line() {
        return this._line;
    };

    equals(coordinate) {
        return this._line === coordinate.get_line() &&
            this._column === coordinate.get_column();
    };

    to_string() {
        if (!this.is_valid()) {
            return "invalid";
        }
        return this._line + " " + this._column;
    }
}

export default Coordinates;